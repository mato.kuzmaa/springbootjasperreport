package com.springboot.Spring.boot.jasper.report;

import com.springboot.Spring.boot.jasper.report.entity.Employee;
import com.springboot.Spring.boot.jasper.report.repository.EmployeeRepository;
import com.springboot.Spring.boot.jasper.report.service.ReportService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.List;

@SpringBootApplication
@RestController
public class SpringBootJasperReportApplication {

		@Autowired
		private EmployeeRepository repository;

		@Autowired
		private ReportService service;

		@GetMapping("/getEmployees")
		public List<Employee> getEmployees() {
			return repository.findAll();
		}

		@GetMapping("/report")
		public String generateReport(@RequestAttribute String format, @RequestAttribute String template) throws JRException, FileNotFoundException {
			return service.exportReport(format, template);
		}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJasperReportApplication.class, args);
	}

}
