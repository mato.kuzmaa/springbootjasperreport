package com.springboot.Spring.boot.jasper.report.repository;

import com.springboot.Spring.boot.jasper.report.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
