package com.springboot.Spring.boot.jasper.report.service;

import com.springboot.Spring.boot.jasper.report.entity.Employee;
import com.springboot.Spring.boot.jasper.report.repository.EmployeeRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {

    @Autowired
    private EmployeeRepository repository;


    // http://localhost:8085/report?format=pdf&template=green
    public String exportReport(String reportFormat, String template) throws FileNotFoundException, JRException {
        String path = "C:\\Users\\kuzma_m\\IdeaProjects\\Spring-boot-jasper-report\\src\\main\\resources\\" + template + ".jrxml";
        List<Employee> employees = repository.findAll();

        // load file and compile it
        //File file = ResourceUtils.getFile("classpath:employees.jrxml");

        JasperReport jasperReport = JasperCompileManager.compileReport(path);

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(employees);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy", "Mato");

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, "C:\\Users\\kuzma_m\\Spring Boot data\\Report Zamestnancov\\employees.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\Users\\kuzma_m\\Spring Boot data\\Report Zamestnancov\\employees.pdf");
        }
        return "report generated in path : " + path;
    }
}
